package id.bootcamp.homepagewithmultirecylcerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Orientation
import id.bootcamp.homepagewithmultirecylcerview.databinding.ActivityMainBinding
import id.bootcamp.homepagewithmultirecylcerview.databinding.ItemMenuBesarBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvItemKecil.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        binding.rvItemMedium.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        binding.rvItemBesar.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)

        binding.rvItemKecil.adapter = ItemKecilAdapter()
        binding.rvItemMedium.adapter = ItemMediumAdapter()
        binding.rvItemBesar.adapter = ItemLargeAdapter()
    }
}